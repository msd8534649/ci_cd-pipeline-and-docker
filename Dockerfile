# Use any base image you prefer
FROM python:3.8

# Set the working directory in the container
WORKDIR /app

# Copy the application files to the container
COPY . /app

# Install any dependencies specified in requirements.txt
RUN pip install -r ./src/requirements.txt

# Define the main command to run your application
CMD ["python", "./src/app.py"]