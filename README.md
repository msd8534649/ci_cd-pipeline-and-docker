# Exercise on GitLab CI/CD Pipeline and Docker 
## Random Words Generator



## Project Overview

This project is part of a mini assignment focusing on GitLab CI/CD pipeline and Docker. This simple app is generating phrases of random words.

## Getting Started

The project's main codebase is located in the 'main' branch of this repository.

### Step 1: Execute the App on a Local Computer

#### Fork the Project

1. Forked the project on the local machine.
2. Verified that Python and PIP are working properly.

#### Execute the Application Locally

1. Ensure that Python and PIP are installed.
2. Installed the required packages using the `requirements.txt` file:

    ```bash
    pip install -r requirements.txt
    ```

3. Run the application locally:

    ```bash
    python ./src/app.py
    ```

### Step 2: Containerize the Application

#### Create a Dockerfile

1. Created a file named `Dockerfile` in the project root.
2. Used the `python:3.8` base image for the project.
3. Defined the "main" process using CMD.

#### Build the Docker Image

1. Open a terminal in the project directory.
2. Build the Docker image:

    ```bash
    docker build -t pydemo:v1 .
    ```

3. Run the Docker image:

    ```bash
    docker run -p 5000:5000 pydemo:v1
    ```

#### Screenshots URL

https://gitlab.labranet.jamk.fi/AF0315/msd_mini_3/-/tree/main/Screenshots

### Step 3: Set Up GitLab CI Pipeline

1. Update the GitLab branch and the name of the file to `.gitlab-ci.yml`.
2. Run the two stages "test" and "build" using MSD and the general Runner.

#### URL for test report xml

https://gitlab.labranet.jamk.fi/AF0315/msd_mini_3/-/tree/main/Screenshots



